package com.example;

import com.example.entity.Device;
import com.example.entity.Employer;
import com.example.entity.Phone;
import com.example.repository.DeviceRepository;
import com.example.repository.EmployerRepository;
import com.example.repository.PhoneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author Oleg Ushakov
 * @since 12.04.2017.
 */
@RestController
@RequestMapping(value = "/device")
public class DeviceController {
	@Autowired
	DeviceRepository deviceRepository;
	@Autowired
	PhoneRepository phoneRepository;
	@Autowired
	EmployerRepository employerRepository;

	@RequestMapping(method = RequestMethod.POST)
	public Device saveNewDevice(@RequestBody Device device) {
//		System.out.println("Saving device: " + device);
		Phone phone = phoneRepository.findByNameEquals(device.getPhone().getName());
		Employer employer = employerRepository.findByNameEquals(device.getEmployer().getName());
		Device temp = deviceRepository.findByNumberEquals(device.getNumber());
		if (null != temp)
			device = temp;
		if (null != phone)
			device.setPhone(phone);
		if (null != employer)
			device.setEmployer(employer);
		return deviceRepository.save(device);
	}

	@RequestMapping(method = RequestMethod.GET)
	public Iterable<Device> findAll(){
		return deviceRepository.findAll();
	}

	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public ResponseEntity<Device> findById(@PathVariable String id){
		Device device = deviceRepository.findByNumberEquals(id);
		if (null == device)
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
		else
			return ResponseEntity.ok(device);
	}

}