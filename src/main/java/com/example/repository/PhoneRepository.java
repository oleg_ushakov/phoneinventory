package com.example.repository;

import com.example.entity.Phone;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Oleg Ushakov
 * @since 12.04.2017.
 */
public interface PhoneRepository extends CrudRepository <Phone,Long> {
	Phone findByNameEquals(String name);
}
