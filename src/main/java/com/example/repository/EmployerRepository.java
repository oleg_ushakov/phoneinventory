package com.example.repository;

import com.example.entity.Employer;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Oleg Ushakov
 * @since 12.04.2017.
 */
public interface EmployerRepository extends CrudRepository<Employer,Long> {
	Employer findByNameEquals(String name);
}
