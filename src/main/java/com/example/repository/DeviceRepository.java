package com.example.repository;

import com.example.entity.Device;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Oleg Ushakov
 * @since 12.04.2017.
 */

public interface DeviceRepository extends CrudRepository<Device,Long> {
	Device findByNumberEquals(String number);
}
