package com.example.entity;

import com.example.entity.baseClasses.EntityAbstract;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Oleg Ushakov
 * @since 12.04.2017.
 */

@Entity
@Table (name = "employer")
public class Employer extends EntityAbstract {
	@Column(name="name")
	private String name;

	public Employer() {
	}

	public Employer(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Employer)) return false;

		Employer employer = (Employer) o;

		return getName().equals(employer.getName());
	}

	@Override
	public int hashCode() {
		return getName().hashCode();
	}
}