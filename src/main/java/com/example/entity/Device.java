package com.example.entity;

import com.example.entity.baseClasses.EntityAbstract;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

import javax.persistence.*;

/**
 * @author Oleg Ushakov
 * @since 12.04.2017.
 */

@Entity
@Table(name = "device")
public class Device extends EntityAbstract {
	@Column(name="number")
	@JsonProperty("id")
	private String number;
	@JoinColumn(name="phone_id")
	@ManyToOne(cascade = CascadeType.ALL)
	@JsonProperty("device")
	@JsonUnwrapped
	private Phone phone;
	@JoinColumn(name="employer_id")
	@ManyToOne(cascade = CascadeType.ALL)
	@JsonProperty("name")
	@JsonUnwrapped
	private Employer employer;

	public Device() {
	}

	public Device(String number, Phone phone, Employer employer) {
		this.number = number;
		this.phone = phone;
		this.employer = employer;
	}


	public void setNumber(String number) {
		this.number = number;
	}

	public String getNumber() {
		return number;
	}

	public Phone getPhone() {
		return phone;
	}

	public void setPhone(Phone phone) {
		this.phone = phone;
	}

	public Employer getEmployer() {
		return employer;
	}

	public void setEmployer(Employer employer) {
		this.employer = employer;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Device)) return false;

		Device device = (Device) o;

		if (!getNumber().equals(device.getNumber())) return false;
		if (!getPhone().equals(device.getPhone())) return false;
		return getEmployer() != null ? getEmployer().equals(device.getEmployer()) : device.getEmployer() == null;
	}

	@Override
	public int hashCode() {
		int result = getNumber().hashCode();
		result = 31 * result + getPhone().hashCode();
		result = 31 * result + (getEmployer() != null ? getEmployer().hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
//		return "Device{" +
//				"number='" + number + '\'' +
//				", phone=" + phone +
//				", employer=" + employer +
//				'}';
		return "";
	}
}