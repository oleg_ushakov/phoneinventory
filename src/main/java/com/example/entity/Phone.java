package com.example.entity;

import com.example.entity.baseClasses.EntityAbstract;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Oleg Ushakov
 * @since 12.04.2017.
 */

@Entity
@Table (name = "phone")
public class Phone extends EntityAbstract {
	@Column(name="name")
	@JsonProperty("device")
	private String name;

	public Phone() {
	}

	public Phone(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Phone)) return false;

		Phone phone = (Phone) o;

		return getName().equals(phone.getName());
	}

	@Override
	public int hashCode() {
		return getName().hashCode();
	}
}