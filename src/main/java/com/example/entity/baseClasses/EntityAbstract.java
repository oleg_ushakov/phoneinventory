package com.example.entity.baseClasses;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Date;

@MappedSuperclass
public class EntityAbstract {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="ID")
	@JsonIgnore
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="MODIFYDATE")
	@JsonIgnore
	private Date modifyDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATEDATE")
	@JsonIgnore
	private Date createDate;

	@Column(name="VERSIONTYPE", columnDefinition = "int default 0")
	@JsonIgnore
	private VersionType versionType = VersionType.CREATE;

	@PreUpdate
	void onPreUpdate() {
		this.setModifyDate(new Date());
	}

	@PrePersist
	void onPreCreate() {
		Date now = new Date();
		this.setCreateDate(now);
		this.setModifyDate(now);
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setVersionType(VersionType versionType) {
		this.versionType = versionType;
	}

	public VersionType getVersionType() {
		return versionType;
	}
}