package com.example.entity.baseClasses;

public enum VersionType {
	CREATE, UPDATE, DELETE
}