import com.example.Application;
import com.example.entity.Device;
import com.example.entity.Employer;
import com.example.entity.Phone;
import com.example.repository.DeviceRepository;
import com.example.repository.EmployerRepository;
import com.example.repository.PhoneRepository;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * @author Oleg Ushakov
 * @since 12.04.2017.
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class RestControllerTest {
	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(),
			Charset.forName("utf8"));

	private MockMvc mockMvc;
	private HttpMessageConverter mappingJackson2HttpMessageConverter;

	@Autowired
	DeviceRepository deviceRepository;
	@Autowired
	PhoneRepository phoneRepository;
	@Autowired
	EmployerRepository employerRepository;
	@Autowired
	private WebApplicationContext webApplicationContext;
	@Autowired
	void setConverters(HttpMessageConverter<?>[] converters) {
		this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream().filter(
				hmc -> hmc instanceof MappingJackson2HttpMessageConverter).findAny().get();

		Assert.assertNotNull("the JSON message converter must not be null",
				this.mappingJackson2HttpMessageConverter);
	}

	@Before
	public void setup() throws Exception {
		this.mockMvc = webAppContextSetup(webApplicationContext).build();
		this.deviceRepository.deleteAll();
		this.employerRepository.deleteAll();
		this.phoneRepository.deleteAll();
		this.deviceRepository.save(new Device("R38G20GJABC", new Phone("Nokia"), new Employer("Пётр Петров")));
		this.deviceRepository.save(new Device("C453476529837", new Phone("Motorola"), new Employer("Сергеев Николай")));
	}

	@Test
	public void userNotFound() throws Exception {
		mockMvc.perform(get("/device/A123456")
				.contentType(contentType))
				.andExpect(status().isNotFound());
	}
	@Test
	public void findAllDevice() throws Exception {
		mockMvc.perform(get("/device/")
				.contentType(contentType))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(2)))
				.andExpect(jsonPath("$[0].id", is("R38G20GJABC")))
				.andExpect(jsonPath("$[0].device", is("Nokia")))
				.andExpect(jsonPath("$[0].name", is("Пётр Петров")))
				.andExpect(jsonPath("$[1].id", is("C453476529837")))
				.andExpect(jsonPath("$[1].device", is("Motorola")))
				.andExpect(jsonPath("$[1].name", is("Сергеев Николай")))
		;
	}
	@Test
	public void saveNewNullDevice() throws Exception {
		Device newDevice = new Device();
		mockMvc.perform(post("/device/")
				.content(this.json(newDevice))
				.contentType(contentType))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id", isEmptyOrNullString()))
				.andExpect(jsonPath("$.device", isEmptyOrNullString()))
				.andExpect(jsonPath("$.name", isEmptyOrNullString()))
		;
	}
	@Test
	public void saveNewDevice() throws Exception {
		Device newDevice = new Device();
		mockMvc.perform(post("/device/")
				.content("{\"id\": \"123456789\", \"device\": \"Ericsson\", \"name\": \"Семён\"}")
				.contentType(contentType))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id", is("123456789")))
				.andExpect(jsonPath("$.device", is("Ericsson")))
				.andExpect(jsonPath("$.name", is("Семён")))
		;
	}
	@After
	public void clean() throws Exception {
		this.deviceRepository.deleteAll();
		this.employerRepository.deleteAll();
		this.phoneRepository.deleteAll();
	}

	protected String json(Object o) throws IOException {
		MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
		this.mappingJackson2HttpMessageConverter.write(
				o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
		return mockHttpOutputMessage.getBodyAsString();
	}
}
